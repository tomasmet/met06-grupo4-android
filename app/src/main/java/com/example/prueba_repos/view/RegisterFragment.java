package com.example.prueba_repos.view;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;
import com.example.prueba_repos.viewmodel.RegisterViewModel;
import com.google.android.material.snackbar.Snackbar;

import static com.example.prueba_repos.model.UserUtils.*;


public class RegisterFragment extends Fragment {
    private EditText edtUserName;
    private EditText edtUserHardware;
    private EditText edtUserMail;
    private EditText edtUserPwd;
    private Button btnRegister;
    private RegisterViewModel viewModel = new RegisterViewModel();
    private UserUtils userUtils = new UserUtils();

    public RegisterFragment() {
        //Required empty public constructor
    }

    public void saveHwId(String uid, String hwId){
        //Create file "dbHardwareId" in mode private so could only be accessed by our APP.
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_PREFERENCES, getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(uid,hwId); //Save HardwareID associated to a UserID.
        userUtils.printLog(TAG_REGISTER_FRAGMENT,sharedPreferences.getAll().toString());
        editor.commit(); //Completes process of creating file and introducing data.
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false); //Assign layout.
        initViewModel();
        bindViews(v);
        return (v);
    }

    private void bindViews(final View view) {
        edtUserName = view.findViewById(R.id.edtUsernameFrReg);
        edtUserHardware = view.findViewById(R.id.edtHardwareFrReg);
        edtUserMail = view.findViewById(R.id.edtUsermailFrReg);
        edtUserPwd = view.findViewById(R.id.edtUserpassFrReg);
        edtUserPwd.setTransformationMethod(new PasswordTransformationMethod());
        btnRegister = view.findViewById(R.id.btnRegisterFrReg);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //on click, send values to the viewmodel
                String userName = edtUserName.getText().toString();
                String userHw = edtUserHardware.getText().toString();
                String userEmail = edtUserMail.getText().toString();
                String userPass = edtUserPwd.getText().toString();
                
                if (userName.isEmpty() || userHw.isEmpty() || userEmail.isEmpty() || userPass.isEmpty()) {
                    Snackbar.make(v, MESSAGE_EMPTY_FIELDS, Snackbar.LENGTH_LONG).show();
                }else{
                    if (userPass.length() >= 6){
                        //Sends user parameters to Firebase Repository through ViewModel.
                        viewModel.registerParam(userName, userHw, userEmail, userPass);
                        //Connects the observer with the observable.
                        viewModel.isRegistered().observe(getViewLifecycleOwner(), registerObserver);
                    }
                }
            }
        });
    }

    private final Observer<Boolean> registerObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(Boolean registered) {
            //Registered is returned by observable when observable changes.
            if(!registered){
                userUtils.printLog(TAG_REGISTER_FRAGMENT,MESSAGE_REGISTER_FAILED);
                Snackbar.make(getView(), MESSAGE_REGISTER_FAILED, Snackbar.LENGTH_LONG).show();
            }else{
                //Shows a message to user to inform the register has been successful.
                Snackbar.make(getView(), MESSAGE_REGISTER_SUCCESSFUL, Snackbar.LENGTH_LONG).show();
                //Saves HW ID to the Shared Preferences file.
                saveHwId(viewModel.getUserID(),edtUserHardware.getText().toString());
                //Navigates to login fragment.
                Navigation.findNavController(getView()).navigate(R.id.action_registerFragment_to_loginFragment);
            }
        }
    };

    public void initViewModel() {
        viewModel.isRegistered().observe(getViewLifecycleOwner(),registerObserver);
    }
}
