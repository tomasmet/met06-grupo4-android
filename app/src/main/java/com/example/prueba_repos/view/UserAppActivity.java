package com.example.prueba_repos.view;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.prueba_repos.model.MyFirebaseMessagingService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.prueba_repos.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.messaging.FirebaseMessagingService;

public class UserAppActivity extends AppCompatActivity {
    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    //Selects the fragment shown depending on the option selected in the lower menu.
                    Fragment selectedFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.monitoring:
                            selectedFragment = new MonitoringFragment();
                            break;

                        case R.id.valveMovement:
                            selectedFragment = new ValveMovementFragment();
                            break;

                        case R.id.pillAdmin:
                            selectedFragment = new CalendarFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_options, selectedFragment).commit();
                    return true;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_app);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_menu); //In the lower fragment, assign menu view.
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener); //Which view will be shown in the fragment.
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_options, new MonitoringFragment()); //Show temp monitoring on first instance.
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    }
}
