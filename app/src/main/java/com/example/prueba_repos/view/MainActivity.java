package com.example.prueba_repos.view;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;

import static com.example.prueba_repos.model.UserUtils.MESSAGE_MAIN_ACTIVITY;
import static com.example.prueba_repos.model.UserUtils.TAG_MAIN_ACTIVITY;

public class MainActivity extends AppCompatActivity {
    private UserUtils userUtils = new UserUtils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        userUtils.printLog(TAG_MAIN_ACTIVITY,MESSAGE_MAIN_ACTIVITY);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
