package com.example.prueba_repos.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;
import com.example.prueba_repos.viewmodel.MonitoringViewModel;

import static com.example.prueba_repos.model.UserUtils.COLOR_RED;
import static com.example.prueba_repos.model.UserUtils.COLOR_WHITE;
import static com.example.prueba_repos.model.UserUtils.FILE_SHARED_PREFERENCES;
import static com.example.prueba_repos.model.UserUtils.LOAD_HWID_ERROR;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_FALL;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_NO_PRESENCE;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_PRESENCE;
import static com.example.prueba_repos.model.UserUtils.TAG_MONITORING_FRAGMENT;

public class MonitoringFragment extends Fragment {
    private String uid;
    private Boolean tempAlert = false;
    private Boolean monitorAlert = false;
    private TextView tvState, tvTemp, tvPresence;
    private ImageView ivStateMonitor;
    private MonitoringViewModel viewModel = new MonitoringViewModel();
    private UserUtils userUtils = new UserUtils();


    public MonitoringFragment() {
    }

    public String loadHwId(){
        uid = viewModel.getUserID();
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String hwId = sharedPreferences.getString(uid,LOAD_HWID_ERROR);
        userUtils.printLog(TAG_MONITORING_FRAGMENT,hwId);
        return hwId;
    }

    //Alert state when temperature is too high or fall detected.
    public void isAlert(Boolean tempAlert, Boolean monitorAlert){
        if(tempAlert || monitorAlert)
            ivStateMonitor.setImageDrawable(getResources().getDrawable(R.drawable.temp_alert));
        else
            ivStateMonitor.setImageDrawable(getResources().getDrawable(R.drawable.check));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_monitoring,container,false);
        initViewModel();
        bindViews(v);
        return v;
    }

    private void initViewModel() {
        viewModel.getTemp(loadHwId()).observe(getViewLifecycleOwner(),tempObserver); //Observe whether temperature has changed with tempObservable.
        viewModel.getPresence(loadHwId()).observe(getViewLifecycleOwner(),presenceObserver); //Observe if there is presence or a falling.
    }

    private void bindViews(final View v) {
        tvState = v.findViewById(R.id.tvState);
        tvTemp = v.findViewById(R.id.tvTemp);
        tvPresence = v.findViewById(R.id.tvPresence);
        ivStateMonitor = v.findViewById(R.id.ivStateMonitor);

        viewModel.getTemp(loadHwId()).observe(getViewLifecycleOwner(), tempObserver);
        viewModel.getPresence(loadHwId()).observe(getViewLifecycleOwner(), presenceObserver);
    }

    private final Observer<Integer> tempObserver = new Observer<Integer>() {
        @Override
        public void onChanged(Integer temp) { //The value sent in postValue() is the one returned in onChanged method (Integer temp in this case)
            tvTemp.setText("Temperature: " + temp.toString() + "º");
            ivStateMonitor.setImageDrawable(getResources().getDrawable(R.drawable.check));
            tvTemp.setTextColor(Color.parseColor(COLOR_WHITE));
            tempAlert = false;

            if(temp>45) {
                tvTemp.setTextColor(Color.parseColor(COLOR_RED));
                ivStateMonitor.setImageDrawable(getResources().getDrawable(R.drawable.temp_alert));
                tempAlert = true;
            }
            isAlert(tempAlert, monitorAlert);
        }
    };

    /*
    If presence = 0 --> no presence
    If presence = 1 --> presence
    If presence = 2 --> WARNING: falling
     */

    //It shows the presence state from Firebase database and interprets it.
    private final Observer<Integer> presenceObserver = new Observer<Integer>() {
        @Override
        public void onChanged(Integer presence) {
            switch (presence){
                case 0:
                    tvPresence.setText(MESSAGE_NO_PRESENCE);
                    tvPresence.setTextColor(Color.parseColor(COLOR_WHITE));
                    monitorAlert = false;
                    break;
                case 1:
                    tvPresence.setText(MESSAGE_PRESENCE);
                    tvPresence.setTextColor(Color.parseColor(COLOR_WHITE));
                    monitorAlert = false;
                    break;
                case 2:
                    tvPresence.setText(MESSAGE_FALL);
                    tvPresence.setTextColor(Color.parseColor(COLOR_RED));
                    monitorAlert = true;
                    break;
            }
            isAlert(tempAlert, monitorAlert);
        }
    };
}
