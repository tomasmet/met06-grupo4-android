package com.example.prueba_repos.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;
import com.example.prueba_repos.viewmodel.ValveMovementViewModel;
import com.google.android.material.snackbar.Snackbar;

import static com.example.prueba_repos.model.UserUtils.*;


public class ValveMovementFragment extends Fragment {
    private String uid;
    private int valvePercent;
    private Button btnValveRight, btnValveLeft;
    private ProgressBar progressBar;
    private TextView tvValvePercent;
    private ValveMovementViewModel viewModel = new ValveMovementViewModel();
    private UserUtils userUtils = new UserUtils();

    public ValveMovementFragment() {
        // Required empty public constructor
    }

    private String loadHwId(){
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(uid,LOAD_HWID_ERROR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_valve_movement, container, false);
        initViewModel();
        bindViews(v);
        return v;
    }

    private void initViewModel() {
        uid = viewModel.getUserID();
        viewModel.getValvePostition(loadHwId()).observe(getViewLifecycleOwner(),percentObserver);
    }

    private void bindViews(final View view) {
        progressBar = view.findViewById(R.id.progress_horizontal);
        btnValveRight = view.findViewById(R.id.btnValveRight);
        btnValveLeft = view.findViewById(R.id.btnValveLeft);
        tvValvePercent = view.findViewById(R.id.tvValvePercent);

        //Gets the position of the valve and sents it to Firebase database through the ViewModel.
        viewModel.getValvePostition(loadHwId()).observe(getViewLifecycleOwner(), percentObserver);

        //Sets the new position to Firebase db through ViewModel when button (open or close) pressed and shows it.
        btnValveRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valvePercent == MAX_VALVE)
                    Snackbar.make(v,MESSAGE_MAX_OPENED,Snackbar.LENGTH_LONG).show();
                else  if (valvePercent < MAX_VALVE){
                    valvePercent++;
                    viewModel.setValvePosition(loadHwId(), valvePercent);

                }
            }
        });
        btnValveLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valvePercent == MIN_VALVE)
                    Snackbar.make(v,MESSAGE_MAX_CLOSED,Snackbar.LENGTH_LONG).show();
                else if (valvePercent > MIN_VALVE){
                     valvePercent--;
                     viewModel.setValvePosition(loadHwId(), valvePercent);
                }
            }
        });
    }

    private final Observer<Integer> percentObserver = new Observer<Integer>() {
        @Override
        public void onChanged(Integer valvePosition) {
            userUtils.printLog(TAG_VALVE_MOVEMENT_FRAGMENT,valvePosition.toString());

            valvePercent = valvePosition; //Sets the received position to the local variable.
            tvValvePercent.setText(valvePosition.toString()); //Sets the received position to the APP Text View.
            progressBar.setProgress(valvePosition); //Sets the received position to the APP Progress Bar.
        }
    };
}
