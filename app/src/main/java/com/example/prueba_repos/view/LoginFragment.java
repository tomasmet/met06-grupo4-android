package com.example.prueba_repos.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;
import com.example.prueba_repos.viewmodel.LoginViewModel;
import com.google.android.material.snackbar.Snackbar;

import static com.example.prueba_repos.model.UserUtils.FILE_SHARED_PREFERENCES;
import static com.example.prueba_repos.model.UserUtils.LOAD_HWID_ERROR;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_EMPTY_EMAIL;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_EMPTY_PASSWORD;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_LOGIN_FAILED;
import static com.example.prueba_repos.model.UserUtils.MESSAGE_PASSWORD_REQUIREMENTS;
import static com.example.prueba_repos.model.UserUtils.TAG_LOGIN_FRAGMENT;
import static com.example.prueba_repos.model.UserUtils.TAG_MONITORING_FRAGMENT;

public class LoginFragment extends Fragment {
    private EditText edtUsermailFrLog, edtUserpassFrLog;
    private Button btnRegisterFrLog, btnLogin;
    private LoginViewModel viewModel = new LoginViewModel();
    private UserUtils userUtils = new UserUtils();

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false); //Assign layout.
        initViewModel();
        bindViews(v);
        return v;
    }


    public void initViewModel(){
        viewModel.hasLogged().observe(getViewLifecycleOwner(),loginObserver);
    }

    private void bindViews(final View view) {
        btnRegisterFrLog = view.findViewById(R.id.btnRegisterFrLog);
        btnLogin = view.findViewById(R.id.btnLogin);
        edtUsermailFrLog = view.findViewById(R.id.edtUsermailFrLog);
        edtUserpassFrLog = view.findViewById(R.id.edtUserpassFrLog);
        edtUserpassFrLog.setTransformationMethod(new PasswordTransformationMethod());

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Gets text from email and password Text Views and assigns them to their objects.
                String userMail =  edtUsermailFrLog.getText().toString();
                String userPassword = edtUserpassFrLog.getText().toString();
                String hwId = loadHwId();

                //Requirement validations.
                if (userMail.isEmpty())
                    Snackbar.make(v, MESSAGE_EMPTY_EMAIL, Snackbar.LENGTH_LONG).show();

                else if(userPassword.isEmpty())
                    Snackbar.make(v, MESSAGE_EMPTY_PASSWORD, Snackbar.LENGTH_LONG).show();

                 else if(userPassword.length()<6)
                    Snackbar.make(v, MESSAGE_PASSWORD_REQUIREMENTS, Snackbar.LENGTH_LONG).show();

                 //Calls method to send email and password to Firebase Repository through ViewModel and, there, log in.
                 else
                    viewModel.loginParam(userMail,userPassword, hwId);
            }
        });

        //When button pressed, navigates to register frame.
        btnRegisterFrLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_registerFragment);
            }
        });
    }


    //Observer gets a TRUE if Firebase could log in the user, and FALSE if could not.
    private final Observer<Boolean> loginObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(Boolean logged) {
            if(!logged){
                //When login not correct, asks user to retry.
                userUtils.printLog(TAG_LOGIN_FRAGMENT,logged.toString());
                Snackbar.make(getView(), MESSAGE_LOGIN_FAILED, Snackbar.LENGTH_LONG).show();
            } else{
                //When login correct, navigates to the user app activity.
                userUtils.printLog(TAG_LOGIN_FRAGMENT,logged.toString());
                Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_userAppActivity);
            }
        }
    };

    public String loadHwId(){
        String uid = viewModel.getUserID();
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String hwId = sharedPreferences.getString(uid,LOAD_HWID_ERROR);
        userUtils.printLog(TAG_MONITORING_FRAGMENT,hwId);
        return hwId;
    }
}
