package com.example.prueba_repos.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.prueba_repos.R;
import com.example.prueba_repos.model.UserUtils;
import com.example.prueba_repos.viewmodel.CalendarViewModel;

import java.util.HashMap;

import static com.example.prueba_repos.model.UserUtils.FILE_SHARED_PREFERENCES;
import static com.example.prueba_repos.model.UserUtils.INIT_CHECK_VALUE;
import static com.example.prueba_repos.model.UserUtils.KEY_FRI;
import static com.example.prueba_repos.model.UserUtils.KEY_MON;
import static com.example.prueba_repos.model.UserUtils.KEY_SAT;
import static com.example.prueba_repos.model.UserUtils.KEY_SUN;
import static com.example.prueba_repos.model.UserUtils.KEY_THU;
import static com.example.prueba_repos.model.UserUtils.KEY_TUE;
import static com.example.prueba_repos.model.UserUtils.KEY_WED;
import static com.example.prueba_repos.model.UserUtils.LOAD_HWID_ERROR;
import static com.example.prueba_repos.model.UserUtils.METHOD_LOAD_HWID;
import static com.example.prueba_repos.model.UserUtils.TAG_CALENDAR_FRAGMENT;

public class CalendarFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private String uid;
    private Button btnAddPlan;
    private Button btnSetPlan;
    private TableLayout tblPlan;

    private TextView tvCheckMonday;
    private TextView tvCheckTuesday;
    private TextView tvCheckWednesday;
    private TextView tvCheckThursday;
    private TextView tvCheckFriday;
    private TextView tvCheckSaturday;
    private TextView tvCheckSunday;

    private TextView tvPillMonday;
    private TextView tvPillTuesday;
    private TextView tvPillWednesday;
    private TextView tvPillThursday;
    private TextView tvPillFriday;
    private TextView tvPillSaturday;
    private TextView tvPillSunday;

    private Spinner spinPillMondayPlan;
    private Spinner spinPillTuesdayPlan;
    private Spinner spinPillWednesdayPlan;
    private Spinner spinPillThursdayPlan;
    private Spinner spinPillFridayPlan;
    private Spinner spinPillSaturdayPlan;
    private Spinner spinPillSundayPlan;

    private String pillMonday;
    private String pillTuesday;
    private String pillWednesday;
    private String pillThursday;
    private String pillFriday;
    private String pillSaturday;
    private String pillSunday;

    private boolean addPlanVisib = false; //Used to show the plan editor when pressed button add plan.

    private UserUtils userUtils = new UserUtils(); //Object used to call method to print messages when debug mode activated.

    private CalendarViewModel viewModel = new CalendarViewModel();

    public CalendarFragment() {
        // Required empty public constructor
    }

    //This method accesses to Shared Preferences file where HWID are saved and gets the one paired with the current user's UID.
    public String loadHwId(){
        userUtils.printLog(TAG_CALENDAR_FRAGMENT,METHOD_LOAD_HWID);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(FILE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(uid,LOAD_HWID_ERROR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calendar, container, false);
        initViewModel();
        bindViews(v);
        return v;
    }

    private void initViewModel() {
        uid = viewModel.getUserID();
        userUtils.printLog(TAG_CALENDAR_FRAGMENT,uid);
        viewModel.getCalendar(loadHwId()).observe(getViewLifecycleOwner(),calendarObserver);
        viewModel.getCheck(loadHwId()).observe(getViewLifecycleOwner(),checkObserver);
    }

    private Observer<HashMap<String, Object>> calendarObserver = new Observer<HashMap<String, Object>>() {
        @Override
        public void onChanged(HashMap<String, Object> calendarMap) {
            userUtils.printLog(TAG_CALENDAR_FRAGMENT,calendarMap.toString());
            //When observer observes changes in calendar pill parameters, sets the database text into Text Views.
            tvPillMonday.setText(calendarMap.get(KEY_MON).toString());
            tvPillTuesday.setText(calendarMap.get(KEY_TUE).toString());
            tvPillWednesday.setText(calendarMap.get(KEY_WED).toString());
            tvPillThursday.setText(calendarMap.get(KEY_THU).toString());
            tvPillFriday.setText(calendarMap.get(KEY_FRI).toString());
            tvPillSaturday.setText(calendarMap.get(KEY_SAT).toString());
            tvPillSunday.setText(calendarMap.get(KEY_SUN).toString());
        }
    };

    private Observer<HashMap<String, Object>> checkObserver = new Observer<HashMap<String, Object>>() {
        @Override
        public void onChanged(HashMap<String, Object> checkCalendarMap) {
            userUtils.printLog(TAG_CALENDAR_FRAGMENT,checkCalendarMap.toString());
            //When observer observes changes in calendar check parameters, sets the database text into Text Views.
            tvCheckMonday.setText(checkCalendarMap.get(KEY_MON).toString());
            tvCheckTuesday.setText(checkCalendarMap.get(KEY_TUE).toString());
            tvCheckWednesday.setText(checkCalendarMap.get(KEY_WED).toString());
            tvCheckThursday.setText(checkCalendarMap.get(KEY_THU).toString());
            tvCheckFriday.setText(checkCalendarMap.get(KEY_FRI).toString());
            tvCheckSaturday.setText(checkCalendarMap.get(KEY_SAT).toString());
            tvCheckSunday.setText(checkCalendarMap.get(KEY_SUN).toString());
        }
    };

    private void bindViews(View v) {
        //Assign each Text View object to the fragment's Text View by its ID.
        tvPillMonday = v.findViewById(R.id.tvPillMonday);
        tvPillTuesday = v.findViewById(R.id.tvPillTuesday);
        tvPillWednesday = v.findViewById(R.id.tvPillWednesday);
        tvPillThursday = v.findViewById(R.id.tvPillThursday);
        tvPillFriday = v.findViewById(R.id.tvPillFriday);
        tvPillSaturday = v.findViewById(R.id.tvPillSaturday);
        tvPillSunday = v.findViewById(R.id.tvPillSunday);

        tvCheckMonday = v.findViewById(R.id.chMonday);
        tvCheckTuesday = v.findViewById(R.id.chTuesday);
        tvCheckWednesday = v.findViewById(R.id.chWednesday);
        tvCheckThursday = v.findViewById(R.id.chThursday);
        tvCheckFriday = v.findViewById(R.id.chFriday);
        tvCheckSaturday = v.findViewById(R.id.chSaturday);
        tvCheckSunday = v.findViewById(R.id.chSunday);

        //Assign each Button object to the fragment's Button by its ID.
        btnAddPlan = v.findViewById(R.id.btnAddPlan);
        btnSetPlan = v.findViewById(R.id.btnSetPlan);
        tblPlan = v.findViewById(R.id.tblPlan); //Assign Table object to the frame's Table by its ID.

        //Assign each Spinner object to the fragment's Spinner by its ID.
        spinPillMondayPlan = v.findViewById(R.id.spinPillMondayPlan);
        spinPillTuesdayPlan = v.findViewById(R.id.spinPillTuesdayPlan);
        spinPillWednesdayPlan = v.findViewById(R.id.spinPillWednesdayPlan);
        spinPillThursdayPlan = v.findViewById(R.id.spinPillThursdayPlan);
        spinPillFridayPlan = v.findViewById(R.id.spinPillFridayPlan);
        spinPillSaturdayPlan = v.findViewById(R.id.spinPillSaturdayPlan);
        spinPillSundayPlan = v.findViewById(R.id.spinPillSundayPlan);

        //Assign each ArrayAdapter object to the fragment's ArrayAdapter by its ID. This is the menu shown when a spinner is pressed, with the 3 pill options.
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers1, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillMondayPlan.setAdapter(adapter1);
        spinPillMondayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers2, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillTuesdayPlan.setAdapter(adapter2);
        spinPillTuesdayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers3, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillWednesdayPlan.setAdapter(adapter3);
        spinPillWednesdayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers4, android.R.layout.simple_spinner_item);
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillThursdayPlan.setAdapter(adapter4);
        spinPillThursdayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers5, android.R.layout.simple_spinner_item);
        adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillFridayPlan.setAdapter(adapter5);
        spinPillFridayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter6 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers6, android.R.layout.simple_spinner_item);
        adapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillSaturdayPlan.setAdapter(adapter6);
        spinPillSaturdayPlan.setOnItemSelectedListener(CalendarFragment.this);

        ArrayAdapter<CharSequence> adapter7 = ArrayAdapter.createFromResource(v.getContext(), R.array.numbers7, android.R.layout.simple_spinner_item);
        adapter7.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPillSundayPlan.setAdapter(adapter7);
        spinPillSundayPlan.setOnItemSelectedListener(CalendarFragment.this);


        btnSetPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When button pressed, text from fragment (pill) is set to a Map. Then the Map is sent to the Firebase database through ViewModel.
                HashMap<String,Object> calendarMap = new HashMap<>();
                calendarMap.put(KEY_MON,pillMonday);
                calendarMap.put(KEY_TUE,pillTuesday);
                calendarMap.put(KEY_WED,pillWednesday);
                calendarMap.put(KEY_THU,pillThursday);
                calendarMap.put(KEY_FRI,pillFriday);
                calendarMap.put(KEY_SAT,pillSaturday);
                calendarMap.put(KEY_SUN,pillSunday);
                userUtils.printLog(TAG_CALENDAR_FRAGMENT,calendarMap.toString());
                viewModel.setCalendar(loadHwId(),calendarMap);

                //When button pressed, text from fragment (check) is set to a Map. Then the Map is sent to the Firebase database through ViewModel.
                HashMap<String,Object> checkMap = new HashMap<>();
                checkMap.put(KEY_MON,INIT_CHECK_VALUE);
                checkMap.put(KEY_TUE,INIT_CHECK_VALUE);
                checkMap.put(KEY_WED,INIT_CHECK_VALUE);
                checkMap.put(KEY_THU,INIT_CHECK_VALUE);
                checkMap.put(KEY_FRI,INIT_CHECK_VALUE);
                checkMap.put(KEY_SAT,INIT_CHECK_VALUE);
                checkMap.put(KEY_SUN,INIT_CHECK_VALUE);
                userUtils.printLog(TAG_CALENDAR_FRAGMENT,checkMap.toString());
                viewModel.setCheck(loadHwId(),checkMap);
            }
        });


        //When button pressed, new plan's table is shown under current plan's table.
        btnAddPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addPlanVisib) {
                    tblPlan.setVisibility(View.VISIBLE);
                    addPlanVisib = true;
                } else {
                    tblPlan.setVisibility(View.INVISIBLE);
                    addPlanVisib = false;
                }

            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //For each spinner, gets the position selected in order to get the String in that position.
        ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

        switch (parent.getId()) {
            case R.id.spinPillMondayPlan:
                pillMonday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillTuesdayPlan:
                pillTuesday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillWednesdayPlan:
                pillWednesday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillThursdayPlan:
                pillThursday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillFridayPlan:
                pillFriday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillSaturdayPlan:
                pillSaturday = parent.getItemAtPosition(position).toString();
                break;

            case R.id.spinPillSundayPlan:
                pillSunday = parent.getItemAtPosition(position).toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
