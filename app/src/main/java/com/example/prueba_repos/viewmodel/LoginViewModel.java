package com.example.prueba_repos.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.prueba_repos.model.FirebaseRepository;
import com.example.prueba_repos.model.UserUtils;

import static com.example.prueba_repos.model.UserUtils.TAG_LOGIN_VIEWMODEL;
import static com.example.prueba_repos.model.UserUtils.TAG_VALVE_MOVEMENT_VIEWMODEL;

public class LoginViewModel extends ViewModel {
    private FirebaseRepository firebaseRepository;
    private MutableLiveData<Boolean> loginCorrectObservable;
    private UserUtils userUtils = new UserUtils();

    public LoginViewModel() {
        firebaseRepository = new FirebaseRepository();
        loginCorrectObservable = new MutableLiveData<>();
    }

    //Returns if user has logged in successfully (TRUE) or not (FALSE)
    public LiveData<Boolean> hasLogged(){
        if(loginCorrectObservable == null){
            loginCorrectObservable = new MutableLiveData<>();
        }
        return firebaseRepository.loginCorrect();
    }

    public void loginParam(String mail, String pwd, String hwId){
        userUtils.printLog(TAG_LOGIN_VIEWMODEL,mail);
        userUtils.printLog(TAG_LOGIN_VIEWMODEL,pwd);
        userUtils.printLog(TAG_LOGIN_VIEWMODEL,hwId);
        firebaseRepository.login(mail, pwd, hwId);
    }

    //Gets current user's UID from Firebase database.
    public String getUserID(){
        String uid = firebaseRepository.getUserID();
        userUtils.printLog(TAG_LOGIN_VIEWMODEL,uid);
        return uid;
    }
}
