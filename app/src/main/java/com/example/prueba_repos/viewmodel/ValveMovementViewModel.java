package com.example.prueba_repos.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.prueba_repos.model.FirebaseRepository;
import com.example.prueba_repos.model.UserUtils;

public class ValveMovementViewModel extends ViewModel {
    private FirebaseRepository firebaseRepository;
    private MutableLiveData<Boolean> valveMovedObservable;
    private UserUtils userUtils = new UserUtils();

    public ValveMovementViewModel() {
        firebaseRepository = new FirebaseRepository();
        valveMovedObservable = new MutableLiveData<>();
    }

    //Gets valve position from Firebase Repository.
    public LiveData<Integer> getValvePostition(String hwId){
        if(valveMovedObservable == null)
            valveMovedObservable = new MutableLiveData<>();
        return firebaseRepository.getValveValue(hwId);
    }

    //Sends valve position to Firebase Repository.
    public void setValvePosition(String hwId, int valvePosition){
        firebaseRepository.setValveDB(hwId, valvePosition);
    }

    //Gets current user's UID from Firebase database.
    public String getUserID(){
        return firebaseRepository.getUserID();
    }
}
