package com.example.prueba_repos.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.prueba_repos.model.FirebaseRepository;
import com.example.prueba_repos.model.UserUtils;

import static com.example.prueba_repos.model.UserUtils.TAG_MONITORING_VIEWMODEL;
import static com.example.prueba_repos.model.UserUtils.TAG_VALVE_MOVEMENT_VIEWMODEL;

public class MonitoringViewModel extends ViewModel {
    private FirebaseRepository firebaseRepository;
    private MutableLiveData<Integer> tempObservable;
    private MutableLiveData<Integer> presenceObservable;
    private UserUtils userUtils = new UserUtils();

    public MonitoringViewModel() {
        firebaseRepository = new FirebaseRepository();
        tempObservable = new MutableLiveData<>();
        presenceObservable = new MutableLiveData<>();
    }

    //Gets temperature value from Firebase database.
    public LiveData<Integer> getTemp(String hwId) {
        if (tempObservable == null)
            tempObservable = new MutableLiveData<>();
        userUtils.printLog(TAG_MONITORING_VIEWMODEL,hwId);
        return firebaseRepository.getTemperature(hwId);
    }

    //Gets presence value from Firebase database.
    public LiveData<Integer> getPresence(String hwId){
        if (presenceObservable == null)
            presenceObservable = new MutableLiveData<>();
        userUtils.printLog(TAG_MONITORING_VIEWMODEL,hwId);
        return firebaseRepository.getPresenceStateDB(hwId);
    }

    //Gets current user's UID from Firebase database.
    public String getUserID(){
        String uid = firebaseRepository.getUserID();
        userUtils.printLog(TAG_VALVE_MOVEMENT_VIEWMODEL,uid);
        return uid;
    }
}
