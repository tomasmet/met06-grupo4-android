package com.example.prueba_repos.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.prueba_repos.model.FirebaseRepository;
import com.example.prueba_repos.model.UserUtils;

public class RegisterViewModel extends ViewModel {
    private FirebaseRepository firebaseRepository;
    private MutableLiveData<Boolean> isRegisteredObservable;
    private UserUtils userUtils = new UserUtils();

    public RegisterViewModel(){
        //Initialize repository object.
        this.firebaseRepository = new FirebaseRepository();
        //Initialize observable object.
        isRegisteredObservable = new MutableLiveData<>();
    }

    //Returns if register has been successful (TRUE) or not (FALSE)
    public LiveData<Boolean> isRegistered(){
        if(isRegisteredObservable == null){
            isRegisteredObservable = new MutableLiveData<>();
        }
        return firebaseRepository.registerUser();
    }

    //Sends user's register parameters to Firebase Repository.
    public void registerParam(String userName, String userHw, String userEmail, String userPass){
        firebaseRepository.register(userName, userHw, userEmail, userPass);
    }

    //Gets current user's UID from Firebase database.
    public String getUserID(){
        return firebaseRepository.getUserID();
    }
}
