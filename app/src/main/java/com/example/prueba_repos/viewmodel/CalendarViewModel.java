package com.example.prueba_repos.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.prueba_repos.model.FirebaseRepository;
import com.example.prueba_repos.model.UserUtils;

import java.util.HashMap;

import static com.example.prueba_repos.model.UserUtils.TAG_CALENDAR_VIEWMODEL;

public class CalendarViewModel extends ViewModel {
    private FirebaseRepository firebaseDatabase;
    private MutableLiveData<HashMap<String,Object>> calendarObservable;
    private MutableLiveData<HashMap<String,Object>> checkObservable;
    private UserUtils userUtils = new UserUtils();

    public CalendarViewModel() {
        firebaseDatabase = new FirebaseRepository();
        calendarObservable = new MutableLiveData<>();
        checkObservable = new MutableLiveData<>();
    }

    //Gets pills plan from the Firebase database.
    public LiveData<HashMap<String,Object>> getCalendar(String hwId){
        if(calendarObservable == null)
            calendarObservable = new MutableLiveData<>();

        userUtils.printLog(TAG_CALENDAR_VIEWMODEL,hwId);
        return firebaseDatabase.getCalendarDB(hwId);
    }

    //Sets pills plan to the Firebase database.
    public void setCalendar(String hwId, HashMap<String,Object> calendarMap){
        userUtils.printLog(TAG_CALENDAR_VIEWMODEL,calendarMap.toString());
        firebaseDatabase.setCalendarDB(hwId, calendarMap);
    }

    //Gets pills check from the Firebase database.
    public LiveData<HashMap<String,Object>> getCheck(String hwId){
        if(checkObservable == null)
            checkObservable = new MutableLiveData<>();

        userUtils.printLog(TAG_CALENDAR_VIEWMODEL,hwId);
        return firebaseDatabase.getCheckDB(hwId);
    }

    //Sets pills plan to the Firebase database.
    public void setCheck(String hwId, HashMap<String,Object> checkMap){
        userUtils.printLog(TAG_CALENDAR_VIEWMODEL,checkMap.toString());
        firebaseDatabase.setCheckDB(hwId, checkMap);
    }

    //Gets current user's UID from Firebase database.
    public String getUserID(){
        return firebaseDatabase.getUserID();
    }
}
