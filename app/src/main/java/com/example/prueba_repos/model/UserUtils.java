package com.example.prueba_repos.model;

/*
This class contains constants and methods used by other classes in the project, as debug methods.
*/

import android.util.Log;

public class UserUtils {
    /*CONSTANTS*/
    public static final String COLOR_WHITE = "#FFFFFF";
    public static final String COLOR_RED = "#FF0000";

    /*Model*/
    //FirebaseRepository
    public static final Boolean DEBUG = Boolean.TRUE; //Debug parameter --> If TRUE, Log messages shown. If FALSE, Log messages not shown.

        //Firebase database keys
    public static final String KEY_NAME = "name";
    public static final String KEY_HARDWARE_ID = "hardwareId";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_TEMPERATURE = "temperature";
    public static final String KEY_VALVE = "valve";
    public static final String KEY_PRESENCE = "presence";
    public static final String KEY_UID = "uid";
    public static final String KEY_TOKEN = "msg_token";

        //Shared Preferences constants
    public static final String FILE_SHARED_PREFERENCES = "dbHardwareId";
    public static final String LOAD_HWID_ERROR = "HardwareID not available";

        //Calendar database keys
    public static final String KEY_MON = "monday";
    public static final String KEY_TUE = "tuesday";
    public static final String KEY_WED = "wednesday";
    public static final String KEY_THU = "thursday";
    public static final String KEY_FRI = "friday";
    public static final String KEY_SAT = "saturday";
    public static final String KEY_SUN = "sunday";

        //Firebase database childs
    public static final String CHILD_USERS = "Users";
    public static final String CHILD_PARAMETERS = "Parameters";
    public static final String CHILD_CALENDAR = "calendar";
    public static final String CHILD_TEMPERATURE = "temperature";
    public static final String CHILD_VALVE = "valve";

        //Firebase database initial values
    public static final int INIT_VALUE_VALVE = 0;
    public static final int INIT_VALUE_TEMP = 0;
    public static final int INIT_VALUE_PRESENCE = 0;
    public static final int INIT_VALUE_PILL = 0;
    public static final int INIT_VALUE_CHECK = 0;

        //Message Values
    public static final String NULL_REGISTER_USER = "null registerUser()";
    public static final String NOT_SUCCESSFUL_TASK = "Task is not successful";
    public static final String METHOD_REGISTER = "register()";
    public static final String NULL_LOGIN_CORRECT = "Null loginObservable in loginCorrect()";
    public static final String METHOD_LOGIN = "login()";
    public static final String METHOD_GET_TEMP = "getTemperature()";
    public static final String NULL_DATA_SNAPSHOT = "Null dataSnapshot";
    public static final String METHOD_GET_VALVE_VALUE = "getValveValue()";
    public static final String METHOD_SET_VALVE_DB = "setValveDB()";
    public static final String METHOD_GET_PRESENCE_STATE_DB = "getPresenceStateDB()";
    public static final String METHOD_GET_CALENDAR_DB = "getCalendarDB()";
    public static final String METHOD_SET_CALENDAR_DB = "setCalendarDB()";
    public static final String METHOD_GET_CHECK_DB = "getCheckDB()";
    public static final String METHOD_SET_CHECK_DB = "setCheckDB()";
    public static final String GET_INSTANCE_ID_FAILED = "getInstanceId failed";



    //MyFirebaseMessagingService
    public static final String ON_MESSAGE_RECEIVED = "onMessageReceived()";
    public static final String NO_NULL_NOTIFICATION = "Notification no Null";


    /*View*/
    //LoginFragment
    public static final String MESSAGE_EMPTY_EMAIL = "Email cannot be empty";
    public static final String MESSAGE_EMPTY_PASSWORD = "Password cannot be empty";
    public static final String MESSAGE_PASSWORD_REQUIREMENTS = "Password must have 6 characters minimum";
    public static final String MESSAGE_LOGIN_FAILED = "Login falied";

    //RegisterFragment
    public static final String MESSAGE_EMPTY_FIELDS = "You must fill all the fields and the password must be longer than 6";
    public static final String MESSAGE_REGISTER_FAILED = "Register failed";
    public static final String MESSAGE_REGISTER_SUCCESSFUL = "Register successful";

    //MonitoringFragment
    public static final String CHILD_PRESENCE = "presence";
    public static final String CHILD_CHECK_CALENDAR = "check_calendar";

    public static final String MESSAGE_NO_PRESENCE = "No presence detected";
    public static final String MESSAGE_PRESENCE = "Presence detected";
    public static final String MESSAGE_FALL = "WARNING:\n FALL DETECTED";

    //ValveMovementFragment
    public static final int MAX_VALVE = 10;
    public static final int MIN_VALVE = 0;
    public static final String MESSAGE_MAX_OPENED = "Valve MAX opened";
    public static final String MESSAGE_MAX_CLOSED = "Valve MAX closed";

    //CalendarFragment
    public static final int INIT_CHECK_VALUE = 0;
    public static final String METHOD_LOAD_HWID = "loadHwId()";

    //MainActivity
    public static final String MESSAGE_MAIN_ACTIVITY = "On create MainActivity";

    /*TAGs*/
    //Model
    public static final String TAG_FIREBASE_REPOSITORY = "FirebaseRepository";
    public static final String TAG_FIREBASE_MESSAGING = "MyFirebaseMessagingService";

    //View
    public static final String TAG_CALENDAR_FRAGMENT = "CalendarFragment";
    public static final String TAG_LOGIN_FRAGMENT = "LoginFragment";
    public static final String TAG_MAIN_ACTIVITY = "MainActivity";
    public static final String TAG_MONITORING_FRAGMENT = "MonitoringFragment";
    public static final String TAG_REGISTER_FRAGMENT = "RegisterFragment";
    public static final String TAG_USER_APP_ACTIVITY = "CalendarFragment";
    public static final String TAG_VALVE_MOVEMENT_FRAGMENT = "CalendarFragment";

    //ViewModel
    public static final String TAG_CALENDAR_VIEWMODEL = "CalendarViewModel";
    public static final String TAG_LOGIN_VIEWMODEL = "LoginViewModel";
    public static final String TAG_MONITORING_VIEWMODEL = "MonitoringViewModel";
    public static final String TAG_REGISTER_VIEWMODEL = "RegisterViewModel";
    public static final String TAG_VALVE_MOVEMENT_VIEWMODEL = "ValveMovementViewModel";


    //METHODS
    public void printLog(String tag, String message){
        if(DEBUG)
            Log.i(tag,message);
    }
}
