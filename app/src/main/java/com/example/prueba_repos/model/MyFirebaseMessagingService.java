package com.example.prueba_repos.model;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.prueba_repos.view.UserAppActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.example.prueba_repos.model.UserUtils.*;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String CHANNEL_ID = "alarma";
    public static final String CHANNEL_NAME = "alarma";

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);

        Log.i(TAG_FIREBASE_MESSAGING, ON_MESSAGE_RECEIVED);
        Log.d(TAG_FIREBASE_MESSAGING, NO_NULL_NOTIFICATION);

        if(remoteMessage.getNotification() != null) {
            Log.d(TAG_FIREBASE_MESSAGING, remoteMessage.getFrom());
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();

            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);

            Notification notification = new NotificationCompat.Builder(this, mChannel.getId())
                    .setContentTitle(title)
                    .setContentText(body)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setSmallIcon(com.google.firebase.R.drawable.notification_template_icon_bg)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .build();

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(mChannel);

            manager.notify(1002, notification);
        }
    }
}


