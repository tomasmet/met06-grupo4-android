package com.example.prueba_repos.model;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.example.prueba_repos.model.UserUtils.CHILD_CALENDAR;
import static com.example.prueba_repos.model.UserUtils.CHILD_CHECK_CALENDAR;
import static com.example.prueba_repos.model.UserUtils.CHILD_PARAMETERS;
import static com.example.prueba_repos.model.UserUtils.CHILD_PRESENCE;
import static com.example.prueba_repos.model.UserUtils.CHILD_TEMPERATURE;
import static com.example.prueba_repos.model.UserUtils.CHILD_USERS;
import static com.example.prueba_repos.model.UserUtils.CHILD_VALVE;
import static com.example.prueba_repos.model.UserUtils.DEBUG;
import static com.example.prueba_repos.model.UserUtils.GET_INSTANCE_ID_FAILED;
import static com.example.prueba_repos.model.UserUtils.INIT_VALUE_CHECK;
import static com.example.prueba_repos.model.UserUtils.INIT_VALUE_PILL;
import static com.example.prueba_repos.model.UserUtils.INIT_VALUE_PRESENCE;
import static com.example.prueba_repos.model.UserUtils.INIT_VALUE_TEMP;
import static com.example.prueba_repos.model.UserUtils.INIT_VALUE_VALVE;
import static com.example.prueba_repos.model.UserUtils.KEY_EMAIL;
import static com.example.prueba_repos.model.UserUtils.KEY_FRI;
import static com.example.prueba_repos.model.UserUtils.KEY_HARDWARE_ID;
import static com.example.prueba_repos.model.UserUtils.KEY_MON;
import static com.example.prueba_repos.model.UserUtils.KEY_NAME;
import static com.example.prueba_repos.model.UserUtils.KEY_PASSWORD;
import static com.example.prueba_repos.model.UserUtils.KEY_PRESENCE;
import static com.example.prueba_repos.model.UserUtils.KEY_SAT;
import static com.example.prueba_repos.model.UserUtils.KEY_SUN;
import static com.example.prueba_repos.model.UserUtils.KEY_TEMPERATURE;
import static com.example.prueba_repos.model.UserUtils.KEY_THU;
import static com.example.prueba_repos.model.UserUtils.KEY_TOKEN;
import static com.example.prueba_repos.model.UserUtils.KEY_TUE;
import static com.example.prueba_repos.model.UserUtils.KEY_UID;
import static com.example.prueba_repos.model.UserUtils.KEY_VALVE;
import static com.example.prueba_repos.model.UserUtils.KEY_WED;
import static com.example.prueba_repos.model.UserUtils.METHOD_GET_CALENDAR_DB;
import static com.example.prueba_repos.model.UserUtils.METHOD_GET_CHECK_DB;
import static com.example.prueba_repos.model.UserUtils.METHOD_GET_PRESENCE_STATE_DB;
import static com.example.prueba_repos.model.UserUtils.METHOD_GET_TEMP;
import static com.example.prueba_repos.model.UserUtils.METHOD_GET_VALVE_VALUE;
import static com.example.prueba_repos.model.UserUtils.METHOD_LOGIN;
import static com.example.prueba_repos.model.UserUtils.METHOD_REGISTER;
import static com.example.prueba_repos.model.UserUtils.METHOD_SET_CALENDAR_DB;
import static com.example.prueba_repos.model.UserUtils.METHOD_SET_CHECK_DB;
import static com.example.prueba_repos.model.UserUtils.METHOD_SET_VALVE_DB;
import static com.example.prueba_repos.model.UserUtils.NOT_SUCCESSFUL_TASK;
import static com.example.prueba_repos.model.UserUtils.NULL_DATA_SNAPSHOT;
import static com.example.prueba_repos.model.UserUtils.NULL_LOGIN_CORRECT;
import static com.example.prueba_repos.model.UserUtils.NULL_REGISTER_USER;
import static com.example.prueba_repos.model.UserUtils.TAG_FIREBASE_REPOSITORY;
import static com.example.prueba_repos.model.UserUtils.TAG_VALVE_MOVEMENT_VIEWMODEL;

public class FirebaseRepository extends AppCompatActivity {
    UserUtils userUtils = new UserUtils(); //Create a UserUtils object to user printLog() when DEBUG = TURE.

    //Firebase objects declarations.
    private FirebaseAuth mAuth; //Instance of Firebase authentication (login)
    private DatabaseReference database; //Instance of Firebase database

    //Observable objects declarations.
    private MutableLiveData<Boolean> loginObservable;
    private MutableLiveData<Boolean> registerObservable;
    private MutableLiveData<Integer> tempObservable;
    private MutableLiveData<Integer> valveObservable;
    private MutableLiveData<Integer> presenceObservable;
    private MutableLiveData<String> tokenMessageObservable;
    private MutableLiveData<HashMap<String, Object>> calendarObservable;
    private MutableLiveData<HashMap<String, Object>> checkObservable;

    private String hardwareId;


    //Maps declarations.
    Map<String, Object> userdata;
    Map<String, Object> userHwData;
    Map<String, Object> calendarMap;
    Map<String, Object> checkCalendarMap;

    //Constructor for FirebaseRepository object.
    public FirebaseRepository() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
    }

    //Getter for Firebase User ID.
    public String getUserID(){
        return mAuth.getCurrentUser().getUid();
    }

    /*----REGISTER----*/
    public LiveData<Boolean> registerUser(){
        if(registerObservable == null){
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,NULL_REGISTER_USER);
            registerObservable = new MutableLiveData<>();
        }
        return registerObservable;
    }

    public void register(final String name, final String hwId, final String email, final String pwd){
        hardwareId = hwId;
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_REGISTER);
        //final String uid; = mAuth.getCurrentUser().getUid();

        //Create a user from email and password with a Firebase own method.
        mAuth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) { // The action above is understood as a task. Then it can be controlled if this task has been successful or not.
                if(!task.isSuccessful()){
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,NOT_SUCCESSFUL_TASK);
                    registerObservable.postValue(Boolean.FALSE);
                } else {
                    FirebaseUser firebaseUser = mAuth.getCurrentUser();
                    if (firebaseUser == null) {
                        userUtils.printLog(TAG_FIREBASE_REPOSITORY, "****Firebase User is NULL");
                        registerObservable.postValue(Boolean.FALSE);
                    }
                    else {
                        String uid = firebaseUser.getUid();
                        userUtils.printLog(TAG_FIREBASE_REPOSITORY, "****current user: " + uid);
                        userdata = new HashMap<>(); //Map for user's register data, which will be included in Firebase database.
                        userHwData = new HashMap<>(); //Map for user hardware parameters data, which will be included in Firebase database.
                        calendarMap = new HashMap<>(); //Map for pills planning, which will be included in Firebase database as value in HW parameters.
                        checkCalendarMap = new HashMap<>(); //Map for pills planning check, which will be included in Firebase database as value in HW parameters

                        //Put user's register data in map userdata Map.
                        userdata.put(KEY_NAME, name);
                        userdata.put(KEY_HARDWARE_ID, hwId);
                        userdata.put(KEY_EMAIL, email);
                        userdata.put(KEY_PASSWORD, pwd);
                        userUtils.printLog(TAG_FIREBASE_REPOSITORY, userdata.toString());


                        //Insert userdata Map in Firebase database.
                        database.child(CHILD_USERS).child(uid).setValue(userdata).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task2) {
                                if (!task2.isSuccessful()) {
                                    userUtils.printLog(TAG_FIREBASE_REPOSITORY, NOT_SUCCESSFUL_TASK);
                                    registerObservable.postValue(Boolean.FALSE);
                                } else {
                                    //User's register data has been inserted correctly in Firebase database.
                                    //In this case, initialize HW parameters in Firebase database.

                                    //Insert initial values to calendar Map
                                    calendarMap.put(KEY_MON, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_TUE, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_WED, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_THU, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_FRI, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_SAT, INIT_VALUE_PILL);
                                    calendarMap.put(KEY_SUN, INIT_VALUE_PILL);
                                    userUtils.printLog(TAG_FIREBASE_REPOSITORY, calendarMap.toString());

                                    //Insert initial values to check Map
                                    checkCalendarMap.put(KEY_MON, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_TUE, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_WED, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_THU, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_FRI, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_SAT, INIT_VALUE_CHECK);
                                    checkCalendarMap.put(KEY_SUN, INIT_VALUE_CHECK);
                                    userUtils.printLog(TAG_FIREBASE_REPOSITORY, checkCalendarMap.toString());

                                    //Insert values to parameters database
                                    userHwData.put(KEY_UID, mAuth.getCurrentUser().getUid());
                                    userHwData.put(KEY_TEMPERATURE, INIT_VALUE_TEMP); //Initialize temperature value to create a child in Firebase Database.
                                    userHwData.put(KEY_PRESENCE, INIT_VALUE_PRESENCE); //Initialize temperature value to create a child in Firebase Database.
                                    userHwData.put(KEY_VALVE, INIT_VALUE_VALVE); //Initialize valve value to create a child in Firebase Database.
                                    userHwData.put(CHILD_CALENDAR, calendarMap); //Insert calendar Map as value for calendar child/key in Firebase database.
                                    userHwData.put(CHILD_CHECK_CALENDAR, checkCalendarMap);
                                    userUtils.printLog(TAG_FIREBASE_REPOSITORY, userHwData.toString());

                                    //Insert HW data into Firebase database.
                                    database.child(CHILD_PARAMETERS).child(hwId).setValue(userHwData).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task3) {
                                            if (!task3.isSuccessful()) {
                                                userUtils.printLog(TAG_FIREBASE_REPOSITORY, NOT_SUCCESSFUL_TASK);
                                                registerObservable.postValue(Boolean.FALSE);
                                            } else {
                                                registerObservable.postValue(Boolean.TRUE);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        getMsgToken(hwId);
                    }
                }
            }
        });
    }


    /*----LOGIN----*/
    public LiveData<Boolean> loginCorrect(){
        if(loginObservable == null){
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,NULL_LOGIN_CORRECT);
            loginObservable = new MutableLiveData<>();
        }
        return loginObservable;
    }

    //If login correct send true, else send false. When true, change activity. When false, ask for data again.
    public void login(String email, String pwd, String hwId){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_LOGIN);
        mAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,NOT_SUCCESSFUL_TASK);
                    loginObservable.postValue(Boolean.FALSE);
                } else
                    loginObservable.postValue(Boolean.TRUE);
            }
        });
        getMsgToken(hwId);
    }


    /*----Temperature Monitoring----*/
    public LiveData<Integer> getTemperature(String hwId){
        hardwareId = hwId;
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_GET_TEMP);
        if(tempObservable == null){
            tempObservable = new MutableLiveData<>();
        }
        if(DEBUG){
            String uid = mAuth.getCurrentUser().getUid(); //Will only check Firebase authentication if mode Debug, to avoid spend time.
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid); //Redundant for this sentence, but necessary to print uid.
        }
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        //Navigate through Firebase database to the Temperature child and get a data snapshot of the database at that point.
        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_TEMPERATURE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot == null){
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,NULL_DATA_SNAPSHOT);
                    tempObservable.postValue(-1);
                }
                else {
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,dataSnapshot.toString());
                    //Database navigates to Temperature child, so dataSnapshot value is actually Temperature value.
                    String temp = dataSnapshot.getValue().toString();
                    tempObservable.postValue(Integer.parseInt(temp));

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                tempObservable.postValue(-1);
            }
        });

        return tempObservable;
    }


    /*----VALVE MOVEMENT----*/

    //Analog to getTemperature() method but accessing to valve child in Firebase database.
    public LiveData<Integer> getValveValue(String hwId){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_GET_VALVE_VALUE);
        if (valveObservable == null)
            valveObservable = new MutableLiveData<>();

        if(DEBUG){
            String uid = mAuth.getCurrentUser().getUid(); //Will only check Firebase authentication if mode Debug, to avoid spend time.
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid); //Redundant for this sentence, but necessary to print uid.
        }

        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_VALVE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot == null){
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,NULL_DATA_SNAPSHOT);
                    valveObservable.postValue(-1);
                }else{
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,dataSnapshot.toString());
                    String valve = (dataSnapshot.getValue().toString());
                    valveObservable.postValue(Integer.parseInt(valve));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                valveObservable.postValue(-1); //In position -> if temp = -1 print error or some.
            }
        });

        return valveObservable;
    }

    //Method used when valve opened/closed from the app instead of the HW. Saves valve value to Firebase database.
    public void setValveDB(String hwId, int valvePercent){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_SET_VALVE_DB);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,Integer.toString(valvePercent));

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_VALVE).setValue(valvePercent);
    }


    /*----PRESENCE & FALLING----*/
    /*
    There are 3 states for presence monitoring:
    1. state = 0 --> no presence
    2. state = 1 --> presence
    3. state = 2 --> WARNING: falling detected
     */
    //This method returns the value of presence sensors as explained above from Firebase database. Analog to getTemperature() method.
    public LiveData<Integer> getPresenceStateDB(String hwId){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_GET_PRESENCE_STATE_DB);
        if(presenceObservable == null)
            presenceObservable = new MutableLiveData<>();

        if(DEBUG){
            String uid = mAuth.getCurrentUser().getUid(); //Will only check Firebase authentication if mode Debug, to avoid spend time.
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid); //Redundant for this sentence, but necessary to print uid.
        }
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_PRESENCE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot == null) {
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,NULL_DATA_SNAPSHOT);
                    presenceObservable.postValue(-1);
                }
                else{
                    String presence = dataSnapshot.getValue().toString();
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY,presence);
                    presenceObservable.postValue(Integer.parseInt(presence));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                presenceObservable.postValue(-1);
            }
        });

        return presenceObservable;
    }


    /*----CALENDAR----*/

    //This method returns pills planification from Firebase database. Which pill for each day in a week.
    public LiveData<HashMap<String,Object>> getCalendarDB(String hwId){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_GET_CALENDAR_DB);

        if(calendarObservable == null)
            calendarObservable = new MutableLiveData<>();

        if(DEBUG){
            String uid = mAuth.getCurrentUser().getUid(); //Will only check Firebase authentication if mode Debug, to avoid spend time.
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid); //Redundant for this sentence, but necessary to print uid.
        }
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_CALENDAR).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userUtils.printLog(TAG_FIREBASE_REPOSITORY,dataSnapshot.toString());
                //Returns the whole week planning as a Map and posts it to the observer.
                calendarObservable.postValue((HashMap<String,Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return calendarObservable;
    }

    //When pill planning set from the app, data is modified in Firebase database.
    public void setCalendarDB(String hwId, HashMap<String, Object> calendarMap){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_SET_CALENDAR_DB);
        if(DEBUG){
            String uid = mAuth.getCurrentUser().getUid(); //Will only check Firebase authentication if mode Debug, to avoid spend time.
            userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid); //Redundant for this sentence, but necessary to print uid.
        }
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        //As in getCalendarDB(), planning is managed as a Map with the whole week planning.
        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_CALENDAR).setValue(calendarMap);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,calendarMap.toString());
    }

    //Analog to getCalendarDB() with check values.
    public LiveData<HashMap<String,Object>> getCheckDB(String hwId){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_GET_CHECK_DB);

        if(checkObservable == null)
            checkObservable = new MutableLiveData<>();

        String uid = mAuth.getCurrentUser().getUid(); //Declare uid at the begining and instanciate it every time is needed.
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_CHECK_CALENDAR).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userUtils.printLog(TAG_FIREBASE_REPOSITORY,dataSnapshot.toString());
                checkObservable.postValue((HashMap<String,Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        return checkObservable;
    }

    //Method used to write in Firebase database the check values for pill planning. It is used to initialize database when user is registered, as the check is done by the HW.
    public void setCheckDB(String hwId, HashMap<String, Object> checkMap){
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,METHOD_SET_CHECK_DB);
        String uid = mAuth.getCurrentUser().getUid(); //Declare uid at the begining and instanciate it every time is needed.
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,uid);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,hwId);

        database.child(CHILD_PARAMETERS).child(hwId).child(CHILD_CHECK_CALENDAR).setValue(checkMap);
        userUtils.printLog(TAG_FIREBASE_REPOSITORY,checkMap.toString());
    }

    /*----NOTIFICATIONS----*/

    /* TODO:
    * Revise Log.w()
    * Implement chain to the view with the tokenMessageObservable
    * implement onNewToken()
    */
    public void getMsgToken(final String hwId) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    userUtils.printLog(TAG_FIREBASE_REPOSITORY, GET_INSTANCE_ID_FAILED);
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();
                database.child(CHILD_PARAMETERS).child(hwId).child(KEY_TOKEN).setValue(token);
            }
        });
    }
}